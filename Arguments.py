import argparse
import sys
import os
import Log_file
import glob

class Argument_pars:
    def create_arg_parser():
        """"Creates and returns the ArgumentParser object."""

        parser = argparse.ArgumentParser(description='Description of your app.')
        parser.add_argument('file',help='File for validation.')

        parsed_args = parser.parse_args()

        if os.path.exists(parsed_args.file):
            (Log_file.log(" File found on : " + os.path.abspath(parsed_args.file)))
        else:
            Log_file.log("File Don't Exist on " + os.getcwd())

    def all_files_in_hierarchy():
        parser = argparse.ArgumentParser(description='Description of your app.')
        parser.add_argument('path', help='Path to the input directory.')
        parsed_args = parser.parse_args()

        for dirpath, dirname, filename in os.walk(parsed_args.path):
            Log_file.log(dirpath)
            Log_file.log(dirname)
            Log_file.log(filename)

    def file_according_to_format():
        parser = argparse.ArgumentParser(description='Description of your app.')
        parser.add_argument('path', help='Path to the input directory.')
        parser.add_argument('file_format', help='Particular file format.')

        parsed_args = parser.parse_args()
        for file in os.listdir(parsed_args.path):
            if file.endswith(parsed_args.file_format):
                 Log_file.log(os.path.join(parsed_args.path, file))



    def file_in_hirarchy():
        parser = argparse.ArgumentParser(description='Description of your app.')
        parser.add_argument('path', help='Path to the input directory.')
        parser.add_argument('file_format', help='Particular file format.')

        parsed_args = parser.parse_args()
        Log_file.log(glob.glob('**/*' + parsed_args.file_format, recursive=True))


arr_obj = Argument_pars
arr_obj.file_in_hirarchy()

