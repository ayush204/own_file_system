import logging
import sys

def log(msg):
        logging.basicConfig(filename='example_log',level=logging.DEBUG, format = "%(asctime)s- %(name)s - %(levelname)s - %(message)s")
        logger = logging.getLogger('simple_example')
        logger.setLevel(logging.DEBUG)
        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        # add formatter to ch
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        # add ch to logger
        logger.addHandler(ch)
        logger.debug(msg)

if __name__ == "__main__":
        log(msg)